// 1. In the s26 folder, create an a1 folder and an activity.js file inside of it.
// 2. Import the http module using the required directive.
// 3. Create a variable port and assign it with the value of 3000.
// 4. Create a server using the createServer method that will listen in to the port provided above.
// 5. Console log in the terminal a message when the server is successfully running.
// 6. Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
// 7. Access the login route to test if it’s working as intended.
// 8. Create a condition for any other routes that will return an error message.
// 9. Access any other route to test if it’s working as intended.
// 10. Create a git repository named S26.
// 11. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 12. Add the link in Boodle.
let http = require("http");

// 3. Create a variable port and assign it with the value of 3000.
const port = 3000;

// 4. Create a server using the createServer method that will listen in to the port provided above.
// 5. Console log in the terminal a message when the server is successfully running.
const server = http.createServer((request, response) => {
// 6. Create a condition that when the login route is accessed, it will print a message to the user that they are in the login page.
// 7. Access the login route to test if it’s working as intended.
    if(request.url == '/login') {
        response.writeHead(200, {'Content-Type': 'text/plain'})
        response.end(`Welcome to the login page.`);
    }
// 8. Create a condition for any other routes that will return an error message.
// 9. Access any other route to test if it’s working as intended.
    else{
        response.writeHead(404, {'Content-Type': 'text/plain'})
        response.end(`Error ${response.statusCode}\nI'm sorry the page you are looking for cannot be found.`);
    }
});

server.listen(port);

// 5. Console log in the terminal a message when the server is successfully running.
console.log(`Server running at localhost: ${port}`);